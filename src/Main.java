import person.Person;
import places.Bar;
import places.Gym;
import places.Work;

public class Main {
    public static void main(String[] args) {

        Person person = new Person(19);
        person.setMoney(100);

        System.out.println(person.toString());

        Bar bar = new Bar();
        bar.visit(person);

        System.out.println(person.toString());

        Gym gym = new Gym();
        gym.visit(person);

        Work work = new Work();
        work.visit(person);



    }
}
