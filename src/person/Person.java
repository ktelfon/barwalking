package person;

public class Person {
    private int age;
    private double money;
    private boolean drunk;
    private int strength;
    private int stamina = 100;

    public Person(int age){
        this.age = age;
    }

    public void getStronger(){
        strength++;
        stamina -= 10;
    }

    public void work(){
        stamina -= 10;
    }

    public int getStamina(){
        return stamina;
    }

    public void rest(){
        stamina = 100;
    }

    public int getAge(){
        return age;
    }

    public void setDrunk(boolean drunk){
        this.drunk = drunk;
    }

    public boolean isDrunk(){
        return drunk;
    }

    public double getMoney(){
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", money=" + money +
                ", drunk=" + drunk +
                '}';
    }
}
